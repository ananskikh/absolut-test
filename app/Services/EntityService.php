<?php

namespace App\Services;

use App\Models\Entity;
use Illuminate\Contracts\Pagination\Paginator;

class EntityService
{
    /**
     * @return mixed|Paginator
     */
    public function getPaginated()
    {
        $paginator = Entity::paginate();
        return $paginator;
    }
}
