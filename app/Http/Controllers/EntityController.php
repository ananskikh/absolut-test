<?php

namespace App\Http\Controllers;

use App\Services\EntityService;

class EntityController extends Controller
{
    /**
     * @var EntityService
     */
    private $entityService;

    /**
     * EntityController constructor.
     *
     * @param EntityService $entityService
     */
    public function __construct(EntityService $entityService)
    {
        // За счет Service Layer инкапсулируем логику поведения модели в Service
        // Но главное - что все дополнительные действия (созданию связанных моделей например), не будут пропущены
        // если все будут использовать этот service layer
        // Контроллер при этом остается максимально чистым
        $this->entityService = $entityService;
    }

    /**
     * Display a listing of the entities.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entities = $this->entityService->getPaginated();
        return view('entity.index', ['entites' => $entities]);
    }
}
