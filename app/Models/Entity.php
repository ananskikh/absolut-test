<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Entity extends Model
{
    /**
     * Entity constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        // Чтобы каждый раз не прописывать в конструкторе, можно было бы вынести установку perPage в DefaultModel
        // А затем наследоваться от DefaultModel. Показалось излишним, так как у нас одна модель сейчас в тестовом задании.
        $this->perPage = Config::get('pagination.per_page');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'content',
    ];

}
