## How to run?

Копируем файл окружения:

```bash
cp .env.example .env
```

База остается на ваше усмотрение. Для быстрого старта я использовал sqlite:

```bash 
touch database/database.sqlite
```

Не забываем настроить доступы к DB после этого в файле .env (Или ваши данные к MySQL например):

```bash
DB_CONNECTION=sqlite
DB_DATABASE=/path/to/project/database/database.sqlite
```

Выполняем миграции:

```bash
php arttisan migrate
```

Заполняем базу тестовыми данными:

```bash
php artisan db:seed
```

Запускаем:

```php
php artisan serve
```

Настройки для пагинации в .env файле:

```php
PAGINATION_PER_PAGE=10
PAGINATION_ACTIVE_LINKS=3
```

### Profit!
