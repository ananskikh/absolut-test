<?php

return [
    'per_page' => env('PAGINATION_PER_PAGE', 10),
    'active_links' => env('PAGINATION_ACTIVE_LINKS', 3)
];
