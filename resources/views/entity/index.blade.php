@extends('layouts.app')

@section('content')
    @foreach($entites as $entity)
        <article>
            <h2>{{ $entity->id }}. {{ $entity->name }}</h2>
            <div>
                {{ $entity->content }}
            </div>
        </article>
        <hr>
    @endforeach

    {{ $entites->links() }}
@endsection
