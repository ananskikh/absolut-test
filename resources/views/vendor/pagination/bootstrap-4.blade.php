@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.first')">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->url(1) }}" rel="first" aria-label="@lang('pagination.first')">&lsaquo;</a>
                </li>
            @endif

            {{-- Почему во view? Потому что LengthAwarePaginator не очень гибкий в вопросе выводимых ссылок.
            Не очень красиво, но по сути это не Бизнес-Логика, а просто логика отображения данных.
            Проще реализовать здесь, нежели переопределять LengthAwarePaginator, да и вообще метод paginate вхардкожен в QueryBuilder --}}

            @foreach(range($paginator->currentPage(), ($paginator->currentPage() + config('pagination.active_links')) > $paginator->lastPage() ? $paginator->lastPage() : $paginator->currentPage() + config('pagination.active_links')) as $pageIndex)
                @if ($pageIndex == $paginator->currentPage())
                    <li class="page-item active" aria-current="page"><span class="page-link">{{ $pageIndex }}</span></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ $paginator->url($pageIndex) }}">{{ $pageIndex }}</a></li>
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" rel="last" aria-label="@lang('pagination.last')">&rsaquo;</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.last')">
                    <span class="page-link" aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
